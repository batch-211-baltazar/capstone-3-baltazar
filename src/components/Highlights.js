import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
  return(
    <Row className = "mt-3 mb-3">
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>Sample1</Card.Title>
          <Card.Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut varius iaculis cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent auctor diam urna, in sagittis neque dapibus venenatis. Suspendisse volutpat eros ac semper vehicula. Aliquam fringilla blandit diam, vehicula faucibus odio dapibus ac. Phasellus eget purus ac massa lacinia suscipit id vitae magna. Nullam non mi ac odio tincidunt faucibus.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>Sample2</Card.Title>
          <Card.Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut varius iaculis cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent auctor diam urna, in sagittis neque dapibus venenatis. Suspendisse volutpat eros ac semper vehicula. Aliquam fringilla blandit diam, vehicula faucibus odio dapibus ac. Phasellus eget purus ac massa lacinia suscipit id vitae magna. Nullam non mi ac odio tincidunt faucibus.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <Card.Title>Sample3</Card.Title>
          <Card.Text>
         Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut varius iaculis cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent auctor diam urna, in sagittis neque dapibus venenatis. Suspendisse volutpat eros ac semper vehicula. Aliquam fringilla blandit diam, vehicula faucibus odio dapibus ac. Phasellus eget purus ac massa lacinia suscipit id vitae magna. Nullam non mi ac odio tincidunt faucibus.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    );
}
