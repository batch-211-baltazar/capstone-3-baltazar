
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import './App.css';

function App() {
  return (
   <>
    <AppNavbar/>
    <Container>
        <Home/>
    </Container>
    </>

  );
}

export default App;
